function ButtonBar (g) {
    this.g = g || document.getElementById('c').getContext('2d');
    this.buttons = [];
    this.pages = [];
    this.nextPageButton = new Button("",this.g);
    this.nextPageButton.registerRenderer(r.cycle);
    this.nextPageButton.prerender(); 
    this.addButton(this.nextPageButton);
    this.respondResize();
};
ButtonBar.prototype.respondResize = function () {
    this.w = this.g.canvas.width;
    this.h = this.g.canvas.height*0.21718;
    this.onscreen = argmax(2,argmin(7,Math.round(this.w / 60))); // should scale this depending on this.w
    this.bw = Math.ceil(this.w / this.onscreen);
    this.x = (this.w - this.bw*argmin(this.buttons.length,this.onscreen))/2;
    for(b in this.buttons) {
        var bt = this.buttons[b];
        bt.wa = this.bw;
        bt.ha = this.h;
        bt.respondResize();
    } 
    this.prerender();
    if(this.pages.length != 0)
        this.draw(); 
};
ButtonBar.prototype.addButton = function (b) {
    this.buttons.push(b);
    b.linea = 0.2;
};
ButtonBar.prototype.prerender = function () {
    this.pages = [];
    var numpages = Math.ceil((this.buttons.length-1) / (this.onscreen-1));
    for(this.page = 0; this.page < numpages; this.page += 1) {
        var buffer = renderToCanvas(this.g.canvas.width,this.nextPageButton.h,this);
        this.pages.push(buffer);
    }
    this.page = 0;
};
ButtonBar.prototype.draw = function () {
    this.g.drawImage(this.pages[this.page],0,this.g.canvas.height - this.nextPageButton.h);
};
ButtonBar.prototype.render = function (gb) { //can maybe use composite
    var offset = (this.page * (this.onscreen-1));
    var limit = argmin(this.buttons.length,offset + this.onscreen);
    for(var i = offset; i < limit; i += 1) {
         if(this.buttons[i] == this.nextPageButton) {
             gb.drawImage(this.nextPageButton.buffer,this.x+this.bw*(limit-offset),0);
             this.nextPageButton.respondTranslate(this.x+this.bw*(limit-offset),this.g.canvas.height-this.nextPageButton.h);
         }
         else {
             gb.drawImage(this.buttons[i].buffer,this.x+this.bw*(i-offset-1),0);    
             this.buttons[i].respondTranslate(this.x+this.bw*(i-offset-1),this.g.canvas.height-this.nextPageButton.h);  
         }
    }      
};
ButtonBar.prototype.inside = function (ex,ey) {
    return ( ey >= this.g.canvas.height-this.nextPageButton.h );
};
ButtonBar.prototype.respondPush = function (ex,ey ) {
    if(this.nextPageButton.inside(ex,ey)) {
        this.page += 1;
        if(this.page >= this.pages.length)
            this.page = 0;
        this.draw();
        return;
    }
    else {
        var offset = 1 + (this.page * (this.onscreen - 1)); // can precompute to speed up on this handler
        var limit = argmin(this.buttons.length,offset + this.onscreen);
        for(var i = offset; i < limit; i+= 1) {
          var bb = this.buttons[i];
          if (bb.inside(ex,ey)) {
               bb.execute();
          }    
        }
    }
};

