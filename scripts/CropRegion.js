function CropBox (p) {
    this.p = p;
    this.hwRatio = 1.618;
    this.hRatio = 3/4;
    this.maskColor = "rgba(50,50,50,0.9)";
    this.maskInverse = "rgba(255,255,255,1.0)";
    this.strokeColor = "rgba(100,100,100,0.7)";
    this.joinStyle = "bevel";
    this.xor = "xor";
    this.hlmin = 78;
    this.wlmin = 48;
    this.tMin = 20;
    this.tRatio = 8;
    this.img = null;
    this.respondResize();
};
CropBox.prototype.registerImage = function (img) {
    this.img = img;
    this.respondResize();
};
CropBox.prototype.respondResize = function () {
    if ( this.img == null ) {
	    this.pw = this.p.canvas.width;
	    this.ph = this.p.canvas.height;
    }
    else {
            this.pw = this.img.w;
            this.ph = this.img.h;
    }
    this.hl = this.ph*this.hRatio;
    this.wl = this.hl/this.hwRatio;
    this.h = this.h <= this.hl ? this.h : argmax(this.hlmin,this.hl);
    this.w = this.h/this.hwRatio;
    if ( this.w > this.pw ) { 
        if( this.pw < this.wlmin ) {
            this.w = this.wlmin;
            this.h = this.hlmin;
        } 
        else {
            this.w = this.pw;
            this.h = this.w*this.hwRatio;
        }
    }
    this.resizeVolatile();
};
CropBox.prototype.resizeDraw = function () {
    this.respondResize();
    this.redraw();
};
CropBox.prototype.resizeVolatile = function () {
    this.x = (this.pw-this.w)/2;
    this.y = (this.ph-this.h)/2;
    this.proximityThreshold = argmin(this.tMin,this.h/this.tRatio);
};
CropBox.prototype.resizeH = function ( ch ) {
    ch = Math.abs(this.ph - 2*ch);
    if( ch >= this.hlmin && ch <= this.hl ) {
        this.h = ch;
        this.w = this.h/this.hwRatio;
        this.resizeVolatile();
        this.redraw();
    }
};
CropBox.prototype.resizeW = function ( cw ) {
    cw = Math.abs(this.pw - 2*cw);
    if( cw >= this.wlmin && cw <= this.wl ) {
        this.w = cw;
        this.h = this.w*this.hwRatio;
        this.resizeVolatile();
        this.redraw();
    }
};
CropBox.prototype.redraw = function () {
    this.clear();
    this.draw();
};
CropBox.prototype.clear = function () {
    this.p.clearRect(0,0,this.p.canvas.width,this.p.canvas.height);
};
CropBox.prototype.draw = function () { 
    var gco = this.p.globalCompositeOperation;
    this.p.fillStyle = this.maskColor;
    this.p.fillRect(0,0,this.p.canvas.width,this.p.canvas.height);
    this.p.globalCompositeOperation = this.xor;
    this.p.fillStyle = this.maskInverse;
    this.p.fillRect(this.x,this.y,this.w,this.h);
    this.p.globalCompositeOperation = gco;
    this.p.lineJoin = this.joinStyle;
    this.p.strokeStyle = this.strokeColor;
    this.p.lineWidth = this.proximityThreshold/2;
    this.p.strokeRect(this.x,this.y,this.w,this.h);
};
CropBox.prototype.touchvalue = function (mx,my) {
    var dx = argmin(this.proximityThreshold, Math.abs(mx - this.x));
    var dw = argmin(this.proximityThreshold, Math.abs(mx - this.x - this.w ));
    var dy = argmin(this.proximityThreshold, Math.abs(my - this.y));
    var dh = argmin(this.proximityThreshold, Math.abs(my - this.y - this.h ));
    var resulth = dh + dy;
    var resultw = dw + dx;
    if((resulth + resultw) < (this.proximityThreshold<<2)) {
	    if(resulth < resultw)
		return 1;
            else
                return 2;
   }
   else 
       return 0; 
};

