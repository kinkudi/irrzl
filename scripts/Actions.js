function Action(method) {
    this.method = method;
};
Action.prototype.execute = function() {
    this.method();
};
