function CropBox (p) {
    this.p = p;
    this.maskColor = "rgba(50,50,50,0.9)";
    this.maskInverse = "rgba(255,255,255,1.0)";
    this.strokeColor = "rgba(100,100,100,0.7)";
    this.joinStyle = "bevel";
    this.xor = "xor";
    this.hlmin = 78;
    this.wlmin = 48;
    this.respondResize();
};
CropBox.prototype.respondResize = function () {
    this.pw = this.p.canvas.width;
    this.ph = this.p.canvas.height;
    this.h = this.ph*0.5;
    this.w = this.pw*0.5;
    this.resizeVolatile();
};
CropBox.prototype.resizeDraw = function () {
    this.respondResize();
    this.redraw();
};
CropBox.prototype.resizeVolatile = function () {
    this.x = (this.pw-this.w)/2;
    this.y = (this.ph-this.h)/2;
    this.proximityThreshold = 20;
};
CropBox.prototype.resizeH = function ( ch ) {
    ch = Math.abs(this.ph - 2*ch);
    this.h = ch;
    this.resizeVolatile();
    this.redraw();
};
CropBox.prototype.resizeW = function ( cw ) {
    cw = Math.abs(this.pw - 2*cw);
    this.w = cw;
    this.resizeVolatile();
    this.redraw();
};
CropBox.prototype.redraw = function () {
    this.clear();
    this.draw();
};
CropBox.prototype.clear = function () {
    this.p.clearRect(0,0,this.p.canvas.width,this.p.canvas.height);
};
CropBox.prototype.draw = function () { 
    var gco = this.p.globalCompositeOperation;
    this.p.fillStyle = this.maskColor;
    this.p.fillRect(0,0,this.p.canvas.width,this.p.canvas.height);
    this.p.globalCompositeOperation = this.xor;
    this.p.fillStyle = this.maskInverse;
    this.p.fillRect(this.x,this.y,this.w,this.h);
    this.p.globalCompositeOperation = gco;
    this.p.lineJoin = this.joinStyle;
    this.p.strokeStyle = this.strokeColor;
    this.p.lineWidth = this.proximityThreshold/2;
    this.p.strokeRect(this.x,this.y,this.w,this.h);
};
CropBox.prototype.touchvalue = function (mx,my) {
    var dx = argmin(this.proximityThreshold, Math.abs(mx - this.x));
    var dw = argmin(this.proximityThreshold, Math.abs(mx - this.x - this.w ));
    var dy = argmin(this.proximityThreshold, Math.abs(my - this.y));
    var dh = argmin(this.proximityThreshold, Math.abs(my - this.y - this.h ));
    var resulth = dh + dy;
    var resultw = dw + dx;
    if((resulth + resultw) < (this.proximityThreshold<<2)) {
	    if(resulth < resultw)
		return 1;
            else
                return 2;
   }
   else 
       return 0; 
};

