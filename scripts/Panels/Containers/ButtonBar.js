function ButtonBar (g) {
    this.g = g || document.getElementById('c').getContext('2d');
    this.buttons = [];
    this.page = 0;
    this.pages = [];
    this.linea = 0.2;
    this.nextPageButton = new Button("",this.g);
    var that = this;
    this.nextPageButton.registerAction( new Action ( 
	    function () {
		that.page += 1;
		if(that.page >= that.pages.length)
		    that.page = 0;
		that.draw();
		return;
	    }
    ));
    this.nextPageButton.registerRenderer(r.cycle);
    this.nextPageButton.prerender(); 
    this.respondResize();
};
ButtonBar.prototype.getKeyValues = function () {
    this.bs = this.buttons.length;
    this.onscreen = argmax(2,argmin(5,Math.round(this.w/60)));
    this.bw = Math.round(this.w / this.onscreen);
    if (this.bs > this.onscreen) {
        this.numpages = Math.ceil(this.bs / (this.onscreen-1));
        this.x = (this.w - this.bw*this.onscreen)/2; 
    }
    else {
        this.numpages = 1;
        this.x = (this.w - this.bw*this.bs)/2;
    }
    this.y = this.g.canvas.height - this.h;
};
ButtonBar.prototype.respondResize = function () {
    this.w = this.g.canvas.width;
    this.h = this.g.canvas.height*0.21718;
    this.getKeyValues();
    for(b in this.buttons) {
        var bt = this.buttons[b];
        bt.wa = this.bw;
        bt.ha = this.h;
        bt.linea = this.linea;
        bt.respondResize();
    } 
    if(this.numpages > 1) {
        this.nextPageButton.wa = this.bw;
        this.nextPageButton.ha = this.h;
        this.nextPageButton.linea = this.linea;
        this.nextPageButton.respondResize();
    }
    this.prerender();
    if(this.pages.length != 0)
        this.draw(); 
};
ButtonBar.prototype.addButton = function (b) {
    this.buttons.push(b);
};
ButtonBar.prototype.prerender = function () {
    this.pages = [];
    for(this.page = 0; this.page < this.numpages; this.page += 1) {
        var buffer = renderToCanvas(this.w,this.h,this);
        this.pages.push(buffer);
    }
    this.page = 0;
};
ButtonBar.prototype.draw = function () {
    this.g.clearRect(this.x-2,this.y-2,this.w+4,this.h+4);
    if(this.numpages > 1) {
        var offset = (this.page * (this.onscreen-1));
        var limit = offset + this.onscreen - 1;
        if ( limit >= this.bs )
           limit = this.bs; 
        var x = (this.w - this.bw*(limit-offset+1))/2;
        this.nextPageButton.respondTranslate(x+this.bw*(limit-offset),this.g.canvas.height-this.nextPageButton.h);
    }
    this.g.drawImage(this.pages[this.page],0,this.y);
};
ButtonBar.prototype.render = function (gb) { //can maybe use composite
    var offset = (this.page * (this.onscreen-1));
    var limit = offset + this.onscreen - 1;
    if ( limit >= this.bs )
       limit = this.bs; 
    var x = (this.w - this.bw*(limit-offset))/2;
    if(this.numpages > 1) {
           var x = (this.w - this.bw*(limit-offset+1))/2;
           gb.drawImage(this.nextPageButton.buffer,x+this.bw*(limit-offset),0);
    }
    for(var i = offset; i < limit; i += 1) {
             gb.drawImage(this.buttons[i].buffer,x+this.bw*(i-offset),0);  
             this.buttons[i].respondTranslate(x+this.bw*(i-offset),this.g.canvas.height-this.buttons[i].h);  
    }      
};
ButtonBar.prototype.inside = function (ex,ey) { // could refine this to check x value which shld be upated on every page
    return ( ey >= this.g.canvas.height-this.h );
};
ButtonBar.prototype.respondPush = function (ex,ey ) {
    // can wrap this in a button action
    if(this.nextPageButton.inside(ex,ey)) {
        this.nextPageButton.execute();
    }
    else {
	 var offset = (this.page * (this.onscreen-1));
	 var limit = offset + this.onscreen - 1;
	 if ( limit >= this.bs )
	       limit = this.bs; 
        for(var i = offset; i < limit; i+= 1) {
          var bb = this.buttons[i];
          if (bb.inside(ex,ey)) {
               bb.execute();
          }    
        }
    }
};

