function Channels () {
     this.locks = {};
};
Channels.prototype.registerLock = function (name) {
      if(this.locks[name] == undefined)
          this.locks[name] = 0; // goes to 1 when an event using name is occurring
};
Channels.prototype.test = function (name) {
      return this.locks[name];
};
Channels.prototype.lock = function (name,priority) {
	//alert("Locking at " + priority);
      this.locks[name] = priority;
};
Channels.prototype.unlock = function (name) {
      this.locks[name] = 0;
}; 
var channels = new Channels();



