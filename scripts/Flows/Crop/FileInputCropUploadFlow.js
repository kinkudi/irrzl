var ixj = document.getElementById('ixj');
function fileSelectHandler(e) {
    files = e.target.files;
    var len = files.length;
    for(var filei = 0; filei < len; filei += 1) {
            var aFile = files[filei];
	    var fileReader = new FileReader();
	    fileReader.onload = (function (theFile) {
		return function(e) {
			if(e.target.result != null && e.target.result != undefined) {
			    var imge = new Image();
			    imge.src = e.target.result;
			    imageDataURLs[theFile.name] = imge;
                            imge.onload = (function () {
                                 return function(e) {
				    var imp = new ImagePreview(gxj,imge);
				    /**registerResizable('imp',imp);
                                    registerDrawable(0,'imp',imp);
                                    gxj.clearRect(0,0,gxj.canvas.width,gxj.canvas.height);
                                    updateFlow();**/
                                    imp.draw();
                                 };
                            })();
			}
		 };
	    })(aFile);
            fileReader.readAsDataURL(aFile);
    }    
};

var fileInputFlow = new Flow();
fileInputFlow.registerLaunchAction ( new Action ( 
    function () {
    ixj.style.display = "inline";
    ixj.style.opacity = "0.0";
    ixj.respondResize = function () {
       var fs = window.innerWidth/25;
       var tht = Math.round(window.innerHeight*0.2);
       this.style.position = "fixed";
       this.style.top = (window.innerHeight-tht)*0.9;
       this.style.left ="10%";
       this.style.textHeight = tht+"px";
       this.style.fontSize = tht+"px";
       this.style.zIndex = "12";
    };
    ixj.respondResize();
    var bxj = document.getElementsByTagName('body')[0];
    var imageDataURLs = {};
    }
));
fileInputFlow.registerListener(ixj,'change', fileSelectHandler );

/**

registerResizable('canvas',cxj);
registerResizable('input',ixj);
var rrr = new Renderer();
window.addEventListener('resize',resizeHandlerI);
ixj.addEventListener('change',fileSelectHandler);
var bigbutton = new Button(gxj,0.5,0.05,0.4,0.9);
bigbutton.registerRenderer(rrr.stick);
bigbutton.respondResize();
bigbutton.prerender();
bigbutton.draw();
var uploadbutton = new iButton(gxj,0.5,0.95,0.3,0.2);
uploadbutton.registerRenderer(rrr.cloud,0.2);
uploadbutton.registerRenderer(rrr.littlePhoto,-0.1,0.25);
uploadbutton.prerender();

var selectbutton = new iButton( gxj,0.5,0.95,0.3,0.2);
selectbutton.registerRenderer(rrr.threePhoto);
selectbutton.prerender();
var cropbutton = new iButton( gxj,0.5,0.95,0.3,0.2);
cropbutton.registerRenderer(rrr.cropcon);
cropbutton.prerender();
registerResizable('cropbutton',cropbutton);
registerResizable('selectbutton',selectbutton);
registerResizable('uploadbutton',uploadbutton);
registerResizable('bigbutton',bigbutton);
registerDrawable(0,'imp',bigbutton);
registerDrawable(1,'icontrol',selectbutton);
//registerDrawable(1,'icontrol',cropbutton);
//registerDrawable(1,'icontrol',uploadbutton);
selectbutton.draw();
uploadFlow = [selectbutton,cropbutton,uploadbutton];
uploadFlowPoint = 0;
function updateFlow () {
    uploadFlowPoint += 1;
    registerDrawable(1,'icontrol',uploadFlow[uploadFlowPoint]);
    drawAll();
};

resizables = {};
drawables = [{},{},{}];
function registerDrawable( dszs, dszs2, dszo ) {
      drawables[dszs][dszs2] = dszo;
};
function registerResizable (rszs, rszo) {
   resizables[rszs] = rszo; 
};
function resizeHandlerI (e) {
    for(rsi in resizables)
        resizables[rsi].respondResize();
    drawAll();
}; 

function drawAll () {
    dlen = drawables.length;
    for(var i = 0; i < dlen; i+= 1) {
        var drawsnow = drawables[i];
        for(drawsitems in drawsnow)
            drawsnow[drawsitems].draw();
    }
};
**/

