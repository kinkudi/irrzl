function ImagePreview ( gx, imge ) {
    this.g = gx;
    this.ie = imge;
    this.establish();
};
ImagePreview.prototype.establish = function () {
    this.nw = this.ie.width;
    this.nh = this.ie.height;
    this.r = this.nw/this.nh;
    this.respondResize(); 
};
ImagePreview.prototype.respondResize = function () {
    this.clear();
    if(this.r > this.g.canvas.naturalRatio) {
	    this.w = this.g.canvas.width * 1.0;
	    this.h = this.w / this.r;
    }
    else {
	    this.h = this.g.canvas.height * 1.0;
	    this.w = this.h * this.r;
    } 
    this.x = (this.g.canvas.width - this.w)/2;
    this.y = (this.g.canvas.height - this.h)/12;
    this.draw();
};
ImagePreview.prototype.clear = function () {
    if(this.x != undefined && this.y != undefined) {
        this.g.clearRect(this.x-2,this.y-2,this.w+4,this.h+4);
    }
};
ImagePreview.prototype.draw = function () {
    this.g.drawImage(this.ie,this.x,this.y,this.w,this.h);
};

