var r = new Renderer();
var bb = new ButtonBar(g);

var uploadb = new Button();
uploadb.registerRenderer(r.stick);
uploadb.registerRenderer(r.camera,0.1,-0.2);
uploadb.prerender();

var cropcon = new Button();
cropcon.registerRenderer(r.cropcon);
cropcon.prerender();

var everyone = new Button();
everyone.registerRenderer(r.threestick);
everyone.prerender();
everyone.registerAction( new Action ( 
    function () {
        alert("Hey this is everyone");
    }
));

var onewayhi = new Button();
onewayhi.registerRenderer(r.lstick,0.05);
onewayhi.registerRenderer(r.stick,0.2);
onewayhi.prerender();

var mutualhi = new Button(g,500,100);
mutualhi.registerRenderer(r.lstick,0.05);
mutualhi.registerRenderer(r.rstick,-0.05);
mutualhi.prerender();

var arrangemeet = new Button(g,100,300);
arrangemeet.registerRenderer(r.lstick);
arrangemeet.registerRenderer(r.rstick);
arrangemeet.registerRenderer(r.clock);
arrangemeet.prerender();

var gomeet = new Button();
gomeet.registerRenderer(r.lstick,0.1);
gomeet.registerRenderer(r.rstick,-0.1);
gomeet.prerender();

var info = new Button(g,0.1,0.7);
info.registerRenderer(r.i);
info.prerender();

bb.addButton(info);
bb.addButton(everyone);
bb.addButton(onewayhi);
bb.addButton(mutualhi);
bb.addButton(arrangemeet);
bb.addButton(gomeet);
bb.addButton(uploadb);
bb.addButton(cropcon);

bb.respondResize();
bb.draw();

