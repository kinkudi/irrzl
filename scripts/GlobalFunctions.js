Function.prototype.inheritsFrom = function( parentClassOrObject ) {
    if ( parentClassOrObject.constructor == Function ) {
        this.prototype = new parentClassOrObject();
        this.prototype.constructor = this;
        this.prototype.parent = parentClassOrObject.prototype;
    }
    else {
        this.prototype = parentClassOrObject;
        this.prototype.constructor = this;
        this.prototype.parent = parentClassOrObject;
    }
    return this;
};

var renderToCanvas = function(w, h, artist) {
    var buffer = document.createElement('canvas');
    buffer.width = w;
    buffer.height = h;
    artist.render(buffer.getContext('2d'));
    return buffer;
};

var argmax = function( a, b ) {
    return (a < b ? b : a);
};

var argmin = function( a, b ) {
    return (a < b ? a : b);
};

HTMLCanvasElement.prototype.respondResize = function () {
	this.width = window.innerWidth;
	this.height = window.innerHeight;
};
HTMLCanvasElement.prototype.insideW = function (mx) {
        return (mx >= 48 && mx <= this.width-48);
};
HTMLCanvasElement.prototype.insideH = function (my) {
        return (my >= 48 && my <= this.height-48);
};


