var bc = document.getElementById('bc');
bc.style.position = "fixed";
bc.style.zIndex = "102";
var gbc = bc.getContext('2d');
bc.respondResize();

d = new Draggable(gbc);
d.draw();
var mouseDown = 0;

resizables = []
resizables.push(bc);
resizables.push(d);

function dragHandler(e) {
    if(mouseDown == 0)
        return;
    var mx = e.clientX;
    var my = e.clientY;
    if(d.inside(mx,my)) {
       d.respondDrag(mx,my); 
    }
};
function registerDown(e) {
    mouseDown = 1;
    var ox = e.clientX;
    var oy = e.clientY;
    if(d.inside(ox,oy)) 
        d.rememberDragPosition(ox,oy);
};
function registerUp(e) {
    mouseDown = 0;
};
function resizeHandler(e) {
    for(ri in resizables) {
        var dg = resizables[ri];
        if( dg == null || dg == undefined )
            continue;
        else
             dg.respondResize();
    } 
};
document.addEventListener('mousedown', registerDown, false);
document.addEventListener('mouseup', registerUp, false );
document.addEventListener('mousemove', dragHandler, false );
window.addEventListener('resize', resizeHandler, false );

